package agent

import org.knowm.xchange.dto.marketdata.OrderBook

object BotMonitor {
  val exchanges = Config.whereBots.map(Exchanges.loaded(_))
  val bookInfos = exchanges.map(_.pair2bookInfo.values.head)

  def botCreate(bookInfo: BookInfo) = {
    val metaBot = new MetaBot(MetaBot.bestMetaStrategy)//
    val orderBook = bookInfo.orderBook
    val iterAsk = orderBook.getAsks.iterator()
    assert(iterAsk.hasNext)
    metaBot.init(orderBook.getTimeStamp.toInstant, iterAsk.next().getLimitPrice.doubleValue())
    metaBot
  }
  val bots = bookInfos.map(botCreate(_))


}
