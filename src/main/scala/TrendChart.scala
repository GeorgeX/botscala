/**
  * Created by Георгий on 10.11.2018.
  */
package agent

import javafx.scene.{chart => jfxsc}
import java.time.{Instant}
import java.time.temporal.ChronoUnit

import scalafx.Includes._
import scalafx.collections.ObservableBuffer
import scalafx.scene.{Node, Scene}
import scalafx.scene.chart._
import scalafx.scene.layout.StackPane
import scalafx.stage.Stage
import javafx.util.StringConverter

import scala.collection.JavaConversions._

trait Notify[T]
{
  var dispatcher: Dispatcher[T] = null

  def updated(x: T) : Unit

  def unsubscribe() {
    if (dispatcher != null) {
      dispatcher.unsubscribe(this)
      dispatcher = null
    }
  }
}

trait Dispatcher[T]
{
  var list: List[Notify[T]] = Nil

  val thisT = this.asInstanceOf[T]

  def subscribe(n : Notify[T]) {
    n.dispatcher = this
    list ::= n
  }
  def unsubscribe(n : Notify[T]) {
    list = list.filter(_ != n)
    n.dispatcher = null
  }

  def dispatch() = list.foreach(_.updated(thisT))
}


case class Window(info: String, listNodes: Node* )
{
  var exit: Unit => Unit = null

  val stage = new Stage{
    title.value = info
    width = 1800
    height = 1000
    scene = new Scene {
      root = new StackPane {
        children = listNodes
      }
    }
  }
  stage.setOnCloseRequest(e => if (exit != null) exit())
  stage.show
}



class LChart(info: String, size: Int, list: String*) {

  var seqRates: Seq[TimeRate] = _

  var mode = "minute"

  val range = 0 until size

  val deltaYdraw = 200 //ширина Y оси слева
  val xAxis = new NumberAxis()

  xAxis.setLowerBound(0)
  xAxis.setUpperBound(size)

  var lower_time: Instant = _ //low time in data

  var upper_time: Instant = _ //upper time in data

  def labelFormatter = new StringConverter[Number] {
    override def toString(obj: Number): String = {
      if (lower_time != null)
        Utils.timeFormat(lower_time.plusSeconds(stepSec * obj.intValue()), mode)
      else
        "?"
    }
    override def fromString(obj: String): Number = {
      2L
    }

  }

  def numberFormatter = new StringConverter[Number] {
    override def toString(obj: Number): String = {
      f"${obj.doubleValue()}%10.2f"
    }
    override def fromString(obj: String): Number = {
      obj.toDouble
    }
  }

  val yAxis = new NumberAxis()
  yAxis.setTickLabelFormatter(numberFormatter)

  val lineChart = LineChart(xAxis, yAxis)

  lineChart.title = info
  lineChart.animated = false
  yAxis.setAutoRanging(false)
  xAxis.setAutoRanging(false)
  //lineChart.setStyle("-fx-stroke-width: 1px;")
  lineChart.setCreateSymbols(false)
  lineChart.setAlternativeRowFillVisible(false)
  //lineChart.setLegendVisible(false)

  val scaleFactor = 1.5

  def diffTimeSec(from: Instant,to: Instant) = ChronoUnit.SECONDS.between(from, to)

  def allTimeSec = diffTimeSec(seqRates(0).time, seqRates.last.time)

  def scaleNow: Double = allTimeSec.toDouble / diffTimeSec(lower_time, upper_time)

  lineChart.setOnScroll(e => if (e.getDeltaY != 0) scale(e.getDeltaY < 0, (e.getX - deltaYdraw)/ lineChart.getWidth, e))

  def scale(up: Boolean, ratio: Double, e: Any): Unit =
  {

    val mult = scaleNow * {if (up) 1 / scaleFactor else scaleFactor}

    if (mult <= 1)
    {
      lower_time = seqRates(0).time
      upper_time = seqRates.last.time
    }
    else {
      val ratio_time_point = ChronoUnit.SECONDS.addTo(lower_time, (diffTimeSec(lower_time, upper_time) * ratio).toLong)

      val scaleTime = allTimeSec / mult

      lower_time = ratio_time_point.minusSeconds((scaleTime * ratio).toLong)

      if (seqRates(0).time.isAfter(lower_time))
        lower_time = seqRates(0).time

      upper_time = lower_time.plusSeconds(scaleTime.toLong)
    }
    update
  }

  def update(seq: Seq[TimeRate]): Unit =
  {
    lower_time = seq(0).time

    upper_time = seq.last.time

    seqRates = seq

    update
  }

  val charts = list.map(s => new XYChart.Series[Number, Number] {name = s})

  for (i <- range)
    charts.foreach(ch => ch.data() += XYChart.Data[Number, Number](0, 0))

  val buf = new ObservableBuffer[jfxsc.XYChart.Series[Number, Number]]()
  charts.foreach(buf.add(_))

  lineChart.data = buf

  var stepSec = 1L

  def update: Unit ={

    val data = Utils.chartData(seqRates, lower_time, upper_time)

    val diffTime = diffTimeSec(lower_time, upper_time)

    mode = if (diffTime <= 6000) "minute" else if (diffTime <= 360000) "hour" else "day"

    stepSec = diffTime / size

    var currMin = Double.MaxValue
    var currMax = 0d

    for(i <- list.indices) {
      val arr = data(i)

      val lastY = arr.last
      val min = arr.min
      val max = arr.max

      if (max > currMax)
        currMax = max

      if (min < currMin)
        currMin = min

      buf(i).getData.zipWithIndex.foreach {
        case (xy, j) => {
          xy.setXValue(j)
          xy.setYValue(if (j < arr.length) arr(j) else lastY)
        }
      }
    }
    val deltaView = (currMax - currMin) / 20
    val min = currMin - deltaView

    yAxis.setLowerBound(if (min < 0) 0 else min)
    yAxis.setUpperBound(currMax + deltaView)
    xAxis.setTickLabelFormatter(labelFormatter)//only reassign format redraw labels
  }

}

case class MiddleChart(info: String, list: Seq[String] )
  extends LChart(info, Config.chartRange, list:_*) with Notify[BookInfo]{

  var nextTimeUpdate: Instant = null

  def updated(bi: BookInfo): Unit =
  {
    if (nextTimeUpdate == null || Instant.now.isAfter(nextTimeUpdate))
    {
      nextTimeUpdate = Instant.now.plusMillis(1000)
/*!!!
      val seq = bi.middles.buf

      val from = seq.front.time

      val to = if (bi.keepCashMs == Long.MaxValue) seq.last.time else from.plusMillis(bi.keepCashMs)

      update(bi.middles.buf.filter(p => p.time == from || (p.time.isAfter(from) && p.time.isBefore(to))))*/
    }
  }


  def window(info: String) =
  {
    val w = Window(info, lineChart)
    w.exit =  _ => unsubscribe
    this
  }
}