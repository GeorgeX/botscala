package agent

import java.time.Instant
import java.time.temporal.ChronoUnit

import genotype.Configuration.{EvolutionConfig, PopulationConfig}
import genotype.Genes.{DoubleGene, IntGene}
import genotype.{EvolutionFactory, Genetic}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer


object MetaBot{

  val bestMetaStrategy = MetaStrategy(DoubleGene(0.0,10.0,Some(3.782297697674696)),DoubleGene(0.0,10.0,Some(1.8021560673268766)),
    DoubleGene(0.0,10.0,Some(1.0908780556828093)),DoubleGene(0.0,1000.0,Some(711.3006379569844)),IntGene(2,2000,Some(924)),
    IntGene(1,200,Some(44)))

  def calcBestMetaStrategies(trs: Seq[TimeRate]) = {

    val extremeSum = (new Bot(new ExtremumStrategy(Utils.extremums(trs)))).totalSum4(trs,Accounts())
    val result = evalMetaBots(extremeSum, trs)
    result
  }

  def getBestMetaBot4(trs: Seq[TimeRate], botAccounts: Accounts = Accounts()): MetaBot =
  {
    val bestMetaStrategy = calcBestMetaStrategies(trs)(0)
    val metaBot = new MetaBot(bestMetaStrategy)
    metaBot.init( trs(0).time.minusMillis(1000), trs(0).rate)
    println("bestMetaStrategy",bestMetaStrategy)
    metaBot
  }

  def errorMetaBot(strategy: MetaStrategy, trs: Seq[TimeRate], extremeSum: Double) = {
    val sum = new MetaBot(strategy).totalSum4(trs,Accounts())
    if (sum >= extremeSum) {
      println(s"extremum $extremeSum  is lower than value $sum")
      0d
    }
    else
      extremeSum - sum
  }

  def evalMetaBots(extremeSum: Double,  trs: Seq[TimeRate]) = EvolutionFactory(
    candidate = Genetic.MetaStrategyParamsGenerator.generateDefault(),
    populationConfig = PopulationConfig(200),
    evolutionConfig = EvolutionConfig(100),
    errorRate = (s: MetaStrategy) => {errorMetaBot(s, trs, extremeSum)}
  ).run()

}

case class MetaStrategy(rateSensitivity: DoubleGene, volumeSensitivity: DoubleGene, profitSensitivity: DoubleGene,
                        criticalForce: DoubleGene, volumesSize: IntGene, delaySwitching: IntGene)

class MetaBot(val strategy: MetaStrategy) {

  var currentBot: BotStrategyMix = new BotStrategyMix(BotStrategy.bestStrategies(0))
  var currentAccount = Accounts()
  var lastRate: Double = _
  var lastTimeSwitchBot: Instant = _
  var accountBeforeSwitch: Accounts = Accounts()
  var buffer_trs = ArrayBuffer[TimeRate]()

  val lastVolumes = mutable.Queue.empty[Double]

  def averageVolume =  if (lastVolumes.isEmpty) {
        println("lastVolumes.isEmpty")
        0d
     }
    else
      lastVolumes.reduce((a,b) => a*b)

  def add2Volumes(v: Double) = {
    lastVolumes += v
    if (lastVolumes.size > strategy.volumesSize.value.get)
      lastVolumes.dequeue()
  }

  def totalSum4(trs: Seq[TimeRate], accounts: Accounts) = {

    var lastTotal = 0d
    trs.map(tr => lastTotal = process(tr))
    lastTotal
  }

  def init(initTime: Instant, rate: Double): Unit =
  {
    lastVolumes.clear()
    lastRate = rate
    lastTimeSwitchBot = initTime
  }

  def process(tr: TimeRate) ={

    add2Volumes(tr.volume)
    buffer_trs.append(tr)

    if (lastTimeSwitchBot == null)
      init(tr.time,tr.rate)
    else

    if (ChronoUnit.MINUTES.between(lastTimeSwitchBot, tr.time) >= strategy.delaySwitching.value.get)
    {
      val rateForce = tr.rate / lastRate * strategy.rateSensitivity.value.get
      val volumeForce = if (tr.volume > averageVolume)
        tr.volume / averageVolume * strategy.volumeSensitivity.value.get
      else
        1

      val extremeBot = new BotStrategyMix(BotStrategy.optimalStrategy4(buffer_trs))
      val extremeSum = extremeBot.totalSum4(buffer_trs, accountBeforeSwitch)
      val currentSum = currentAccount.currentSum(tr.rate)

      val profitForce = extremeSum / currentSum *
        strategy.profitSensitivity.value.get
      val force = rateForce * volumeForce * profitForce

      if (force >= strategy.criticalForce.value.get)
        changeBot(force / strategy.criticalForce.value.get, extremeBot, tr)
      buffer_trs.clear()
      accountBeforeSwitch = currentAccount.copy
    }
    lastRate = tr.rate
    currentBot.process(tr, currentAccount)
    currentAccount.currentSum(tr.rate)
  }

  def changeBot(forceMulti: Double, botStrategyMix: BotStrategyMix, tr: TimeRate): Unit = {
    lastTimeSwitchBot = tr.time
    currentBot = botStrategyMix
  }
}
