/**
  * Created by Георгий on 05.11.2018.
  */
package agent

import java.time.Instant
import java.time.temporal.ChronoUnit

import scala.collection.Searching._
import scala.collection.mutable
import scalafx.beans.property.ObjectProperty
import scalafx.collections.ObservableBuffer
import scalafx.scene.control._
import TableColumn._
import scalafx.event.ActionEvent
import scalafx.scene.Scene
import scalafx.scene.control.ScrollPane.ScrollBarPolicy
import scalafx.scene.layout.VBox
import scalafx.stage.Stage
import scalafx.Includes._

import scala.collection.JavaConversions._

object TimeSeries
{
  def startRecord(exchange: Exchange): Instant =
  {
    val time = Instant.now
    Db.startWriteSeries(exchange)
    time
  }

  def play(si: SeriesInfo): Unit =

    Exchanges.loaded.get(si.exchange).map(exchange => exchange.play(si))

  def drawRates(timeRates: Seq[TimeRate], si: SeriesInfo, chart: MiddleChart): Unit =
  {
    val exchange = Exchanges.loaded(si.exchange)

    val currency = chart.info

    val bookInfo = new BookInfo(exchange, exchange.specs.find(_.base.toString == currency).get, Long.MaxValue)

    val seq = timeRates.filter(_.param == currency)

    seq.foreach(bookInfo.update(_))

    chart.updated(bookInfo)
  }

  def chartSeries(si: SeriesInfo): Unit =
  {
    //temp for 1 currency
    val currency = "ETH"

    val list = List("small", "middle", "big") ++ Config.baseTrendSumString

    val chart = MiddleChart(currency, list).window(si.exchange)

    Db.readSeriesInfo(si, tsa => drawRates(tsa, si, chart))
  }

  def stopRecord(exchange: Exchange, startTime: Instant): Unit =
  {
    Db.stopWriteSeries(exchange)
    val si = SeriesInfo(exchange.name, startTime, Instant.now, exchange.pair2bookInfo.map(_._1.base.toString).toSeq)
    Db.writeSeriesInfo(si)
    Db.series(exchange.name, table.update)
  }

  val table = new SeriesTable

  val vbox = new VBox()

  vbox.children = table.viewPoint

  val scroll = new ScrollPane(){
    hbarPolicy = ScrollBarPolicy.AsNeeded
    vbarPolicy = ScrollBarPolicy.AsNeeded
    content = vbox
  }

  val stageSeries = new Stage{

    width = 982
    height = 982
    scene = new Scene {
      content = scroll
    }
  }
  //stageOrderBook.setOnCloseRequest(e =>  clearBoxBooks())
  //scroll.setMaxHeight(table.viewPoint.getHeight)

  def showSeries(exchange: String): Unit =
  {
    stageSeries.title = exchange + " series"

    Db.series(exchange, table.update)

    stageSeries.show
  }
}

case class SeriesInfo(exchange: String, start : Instant, end : Instant, currencies: Seq[String])

case class SeriesView(si: SeriesInfo)
{
  val time = new ObjectProperty(this, "start", si.start)
  val duration = new ObjectProperty(this, "duration", ChronoUnit.MILLIS.between(si.start, si.end).toDouble / 1000)
  val label = new ObjectProperty(this, "label", si.currencies.mkString(","))

  val buttonPlay = new Button("Start")
  buttonPlay.onAction = (event: ActionEvent) =>  {
    TimeSeries.play(si)
  }
  val play = new ObjectProperty(this, "start", buttonPlay)

  val buttonView = new Button("Chart")
  buttonView.onAction = (event: ActionEvent) =>  {
    TimeSeries.chartSeries(si)
  }
  val view = new ObjectProperty(this, "view", buttonView)
}

class SeriesTable
{
  val buffer = new ObservableBuffer[SeriesView]()

  def update(asi: Seq[SeriesInfo])
  {
    buffer.clear()
    asi.foreach(si => buffer.append(SeriesView(si)))
    //viewPoint.setItems(buffer)
  }

  val viewPoint = new TableView[SeriesView](buffer)
  {
    columns ++= List(
      new TableColumn[SeriesView, Instant] {
        text = "Begin"
        cellValueFactory = _.value.time
        prefWidth = 290
      },
      new TableColumn[SeriesView, Double] {
        text = "Duration,sec"
        cellValueFactory = _.value.duration
        prefWidth = 290
      },
      new TableColumn[SeriesView, String] {
        text = "Currencies"
        cellValueFactory = _.value.label
        prefWidth = 190
      },
      new TableColumn[SeriesView, Button] {
        text = "Play"
        cellValueFactory = _.value.play
        cellFactory = {
          _: TableColumn[SeriesView, Button] => new TableCell[SeriesView, Button] {
            item.onChange { (_, _, newButton) =>
              graphic = newButton
            }
          }
        }
      },
      new TableColumn[SeriesView, Button] {
        text = "Chart"
        cellValueFactory = _.value.view
        cellFactory = {
          _: TableColumn[SeriesView, Button] => new TableCell[SeriesView, Button] {
            item.onChange { (_, _, newButton) =>
              graphic = newButton
            }
          }
        }
      }
    )
    editable = false
  }
  viewPoint.setPrefHeight(800)
  viewPoint.setPrefWidth(982)
}

//rates for buying/selling 100,1000, 10000 $ currency
case class TimeRate(time: Instant, rates: Array[Double], volume: Double = 0, param: String = null) extends Ordered[TimeRate]
{
  def compare(that: TimeRate) = time.compareTo(that.time)

  def equal(that: TimeRate) = rates.deep == that.rates.deep

  def currencyFrom = "BTC"

  def currencyTo = "USD"

  def rate = rates(0)
}

class TimeSeries
{
  val buffer = mutable.ArrayBuffer[TimeRate]()

  def get(from: Instant, to_ : Instant = null): mutable.ArrayBuffer[TimeRate] =
  {
    val fromIndex = exactIndex(from)._2

    if (to_ != null)
      buffer.slice(fromIndex, exactIndex(to_)._2)
    else
      buffer.drop(fromIndex)
  }

  //return (exact(true) or before(false) position, index)
  def exactIndex(time: Instant) = buffer.search(TimeRate(time, null)) match {
    case Found(index) => (true,index)
    case InsertionPoint(index) => (false,index)
  }

  def add(tr: TimeRate){
    buffer.append(tr)
  }
}


