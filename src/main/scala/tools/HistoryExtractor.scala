package agent

import java.time.Instant

object HistoryExtractor {

  History.limit = 5000
  val bitfinex = "bitfinex"
  val daysCount = 730
  val minutes = daysCount * 24 * 60
  val nowSec = Instant.now.toEpochMilli / 1000
  var timeSec = nowSec - minutes * 60
  val stepSec = History.limit * 60
  var i = 0

  val middles = Middles((1 to 400).map(_ * 1000 * 60))

  def extract() {
    while (timeSec < nowSec)
    {
      timeSec += stepSec
      val optionArr = History.get("bitfinex", timeSec, "BTC").map(_.arr)

      if (optionArr.isDefined)
      {
        i += 1
        println("step=", i)
        val ratesVolumes = optionArr.get
        val rateMiddles = ratesVolumes.map(tr => Array(tr.rates(0)) ++ middles.update(tr))
        Db.writeHistory(ratesVolumes, bitfinex, rateMiddles)
      }
      else {
        timeSec -= stepSec //try again
        Thread.sleep(65000)
        println("refuse", Utils.timeFormat(Instant.ofEpochSecond(timeSec), "day"))
      }
    }
  }
}
