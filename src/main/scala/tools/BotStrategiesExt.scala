package agent

import genotype.Genetic._

object BotStrategiesExt {

  val best28 = Array(defaultBotStrategyParams
    )

  val best14 = Array(defaultBotStrategyParams)
  val best7 = Array(defaultBotStrategyParams)
  val best3p5 = Array(defaultBotStrategyParams)
  val arrList = List(best3p5, best7, best14, best28)

  val names = List("best3p5", "best7", "best14", "best28")

  val bestStrategies = arrList.flatten

}
