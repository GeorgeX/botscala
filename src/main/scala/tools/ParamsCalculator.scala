package agent

import java.time.Instant
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}

object BotsStrategiesCalculator {

  val dateRange = Db.getTimeRangeHistory

  def botEarning4StrategyParams(strategyParams: StrategyParams, trs: Seq[TimeRate]) = {
    val bot = new BotStrategyMix(strategyParams)
    val accounts = Accounts()
    (accounts.currentSum(trs(0).rate), bot.totalSum4(trs, accounts))
  }

  def optimalPeriodParameters(trs: Seq[TimeRate]) : (StrategyParams, Double) = {
    val strategyParams = BotStrategy.optimalStrategy4(trs)
    val (initSum, sum) = botEarning4StrategyParams(strategyParams, trs)
    (strategyParams, sum * 100 / initSum - 100)
  }

  def writePeriodParams(periodMinutes: Long, end: Instant, trs: Seq[TimeRate],exchange: String) = {
    val (strategyParams, percents) = optimalPeriodParameters(trs)
    synchronized{
      maxThread += 1
    }
    Db.writeStrategyParams(end, exchange + periodMinutes, strategyParams, percents)
  }

  def calcWritePeriodParams(periodMinutes: Long, end: Instant, exchange: String = "bitfinex") =
    Db.historyCallback(exchange, end.minusSeconds(periodMinutes * 60), end,
      { trs => writePeriodParams(periodMinutes,end, trs, exchange)})

  var maxThread = 8 //8 threads and not more

  def calcWriteAllParameters4(periodMinutes: Long, exchange: String = "bitfinex"): Unit =
  {
    val (begin, end) = dateRange
    var finish = begin.plusSeconds(periodMinutes * 60)

    while(finish.isBefore(end) || finish == end)
    {
      calcWritePeriodParams(periodMinutes, finish, exchange)
      finish = finish.plusSeconds(periodMinutes * 30) //every half period
      synchronized{
        maxThread -= 1
      }
      while(maxThread <= 0)
        Thread.sleep(500)
    }
  }

  val maxTimeRangeSec = 2000 * 60

  def calcWriteEffect4(sp: StrategyParams, start: Instant, end: Instant, maxEffect: Double, exchange: String)= Future
  {
    var from = start
    val bot = new BotStrategyMix(sp)
    val accounts = Accounts()
    var initSum, lastSum = 0d

    while(from.isBefore(end)){
      val to = from.plusSeconds(maxTimeRangeSec)
      lastSum =  Await.result(Db.historyQuery(exchange, from, to).map(trs => {
        if (from == start)
          initSum = accounts.currentSum(trs(0).rate)
        bot.totalSum4(trs, accounts)
      }), Duration.Inf)
      from = to
    }
    val effect = (lastSum - initSum) / initSum
    Db.writeEffects(start, effect, maxEffect)
    synchronized{
      maxThread += 1
    }
  }
  /*
    def calcMapBestStrategies =
    {
      val s2i = mutable.Map.empty[StrategyParams, Int]
      for(_ <- 0 to 20)
        calcBestStrategies(80).foreach(e => s2i(e._2) = s2i.getOrElse(e._2, 0) + 1)
      s2i.toArray.sortBy(_._2).reverse.foreach(e => println(e._2 -> e._1, ","))
    }

    def calcBestStrategies(steps: Int) = {
      val data = tools.PeriodProfitData.data.sortBy(_._1).reverse
      val tops = data.take(100).map(_._4)

      val bots = tops.map(st => new BotStrategyMix(st))

      val (begin, end) = dateRange

      val stepTimeSec = 2000 * 60

      val rangesMinutes = ChronoUnit.SECONDS.between(begin, end) / stepTimeSec - 1

      val diffs = Array.fill[Double](bots.size)(1d)

      for (i <- 0 to steps) {

        val randomIndex = Random.nextInt(rangesMinutes.toInt)

        val from = begin.plusSeconds(stepTimeSec * randomIndex)
        val to = from.plusSeconds(stepTimeSec)

        val trs = Await.result(Db.historyQuery("bitfinex", from, to), Duration.Inf)

        def calcSumBot(j: Int) = Future {
          val accounts = Accounts()
          val initSum = accounts.currentSum(trs(0).rate)
          val lastSum = bots(j).totalSum4(trs, accounts)
          diffs(j) *= lastSum / initSum
        }
        assert(!trs.isEmpty)

        Await.result(Future.sequence(bots.indices.map(calcSumBot(_))), Duration.Inf)
      }
      val result = diffs.zip(tops).sortBy(_._1).reverse.take(10)
      //result.foreach(println(_,","))
      result
    }
  */

  def calcWriteEffectParameters(periodMinutes: Long, exchange: String = "bitfinex"): Unit =
  {
    val (begin, end) = dateRange
    val mapI2sp = Db.readStrategyParams(begin, end)

    for((t, (sp, _)) <- mapI2sp)
    {
      var endPeriod = t.plusSeconds(periodMinutes * 60)
      if (endPeriod.isAfter(end))
        endPeriod = end
      val seq = mapI2sp.filter(e => e._1.isAfter(t) && e._1.isBefore(endPeriod))
      if (seq.isEmpty) {
        val maxEffect = seq.map(_._2._2 / 100 + 1).reduce(_ * _)
        calcWriteEffect4(sp, t, endPeriod, maxEffect, exchange)
        synchronized {
          maxThread -= 1
        }
        while (maxThread <= 0)
          Thread.sleep(500)
      }
    }
  }
}
