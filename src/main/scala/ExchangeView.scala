/**
  * Created by Георгий on 03.11.2018.
  */
package agent

import java.time.Instant

import org.knowm.xchange.dto.trade.LimitOrder
import org.knowm.xchange.dto.marketdata.{OrderBook, Trade}
import org.knowm.xchange.currency._

import scalafx.Includes._
import scalafx.beans.property.ObjectProperty
import scalafx.collections.ObservableBuffer
import scalafx.event.ActionEvent
import scalafx.scene.control._
import scalafx.scene.control.TableColumn._
import scalafx.scene.layout.{HBox, VBox}
import scala.collection.JavaConversions._

class OrderView
{
  val price = new ObjectProperty(this, "price", 0d)
  val amount = new ObjectProperty(this, "amount", 0d)

  def update(order: LimitOrder) =
  {
    val pr = order.getLimitPrice.doubleValue
    val am = order.getRemainingAmount.doubleValue

    if (price.value == pr && amount.value == am)
      false
    else {
      price.value = pr
      amount.value = am
      true
    }
  }
}

class OrdersView
{
  val buffer = new ObservableBuffer[OrderView]()
  (0 to 20).foreach(i => buffer.append(new OrderView))

  def update(orders: Seq[LimitOrder]): Boolean =
  {
    var res = true
    for(i <- 0 to 20)
    {
      val ures = buffer(i).update(orders(i))
      if (res && !ures)
        res = false
    }
    res
  }

  val viewPoint = new TableView[OrderView](buffer)
  {
    columns ++= List(
      new TableColumn[OrderView, Double] {
        text = "Price"
        cellValueFactory = _.value.price
        prefWidth = 90
      },
      new TableColumn[OrderView, Double] {
        text = "Amount"
        cellValueFactory = _.value.amount
        prefWidth = 90
      }
    )
    editable = false
  }
  viewPoint.setPrefHeight(800)
  viewPoint.setPrefWidth(182)
}

case class PairView(bookInfo: BookInfo)
{
  val asks = new OrdersView
  val bids = new OrdersView

  val button = new Button("Chart")
  button.onAction = (event: ActionEvent) =>  {
    ExchangesTable.showChart(bookInfo)
  }

  val viewPoint = new VBox{children = List(button, new HBox{children = List(
    new VBox{children = List(new Label("bids " + bookInfo.pair), bids.viewPoint)},
    new VBox{children = List(new Label("asks " + bookInfo.pair), asks.viewPoint)})
  })}

  def update()
  {
    val orderBook = bookInfo.orderBook
    asks.update(orderBook.getAsks)
    bids.update(orderBook.getBids)
  }
}

case class ExchangeView(exchange: Exchange)
{
  val name = new ObjectProperty(this, "name", exchange.name)
  val status = new ObjectProperty(this, "status", false)
  val amount = new ObjectProperty(this, "amount", 0d)
  val buttonBoard = new Button("Show board")
  var startTime: Instant = null

  buttonBoard.onAction = (event: ActionEvent) =>  {
    ExchangesTable.showOrderBooks(exchange)
  }
  val board = new ObjectProperty(this, "show", buttonBoard)

  val buttonStart = new Button("Start")
  buttonStart.onAction = (event: ActionEvent) =>  {
    if (startTime == null) {
      buttonStart.text = "Stop"
      startTime = TimeSeries.startRecord(exchange)
    }
    else {
      buttonStart.text = "Start"
      TimeSeries.stopRecord(exchange,startTime)
    }
  }
  val start = new ObjectProperty(this, "start", buttonStart)

  val buttonSeries = new Button("Series")
  buttonSeries.onAction = (event: ActionEvent) =>  {
    TimeSeries.showSeries(exchange.name)
  }
  val series = new ObjectProperty(this, "series", buttonSeries)

  def update(exchange: Exchange) = {

    val am = exchange.amount
    val st = exchange.status

    if (amount.value == am && status.value == st)
      false
    else {
      amount.value = am
      status.value = st
      true
    }
  }
}

case class CurrencyView(currency: Currency)
{
  val name = new ObjectProperty(this, "name", currency.toString)
  val active = new ObjectProperty(this, "active", false)
  val amount = new ObjectProperty(this, "amount", 0d)
  val button = new Button("Show")
  button.onAction = (event: ActionEvent) =>  {
    CurrencyTable.showCurrency(currency)
  }
  val show = new ObjectProperty(this, "show", button)

  def update(exchange: Exchange) =
  {
    val am = exchange.amount
    val st = exchange.status

    if (amount.value == am && active.value == st)
      false
    else {
      amount.value = am
      active.value = st
      true
    }
  }
}


object CurrencyTable
{
  val buffer = new ObservableBuffer[CurrencyView]()
  var exchange: Exchange = null

  def set(exchange_ : Exchange)
  {
    exchange = exchange_
    buffer.clear()
    exchange.pair2bookInfo.keys.foreach(cp => buffer.append(CurrencyView(cp.base)))
  }

  def showCurrency(currency: Currency)
  {
    exchange.pair2bookInfo.keys.find(_.base == currency).foreach(k => openCurrencyView(exchange.pair2bookInfo(k)))
  }

  def openCurrencyView(bookInfo: BookInfo)
  {
  }
  def update(exchange: Exchange) = buffer.foreach(_.update(exchange))
    buffer.find(_.name == exchange.name).map(_.update(exchange))
}
