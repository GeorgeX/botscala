/**
  * Created by Георгий on 14.10.2018.
  */
package agent

import info.bitrich.xchangestream.bitfinex.BitfinexStreamingExchange
import info.bitrich.xchangestream.core._
import info.bitrich.xchangestream.poloniex.PoloniexStreamingExchange
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.marketdata.{OrderBook, Trade}

import scala.language.postfixOps
import scala.language.implicitConversions
import io.reactivex.functions._

import scala.collection.JavaConversions._
import scala.collection.mutable
import scalafx.collections.ObservableBuffer
import scalafx.scene.Scene
import scalafx.scene.control.ScrollPane.ScrollBarPolicy
import scalafx.scene.control._

import scalafx.Includes._
import scalafx.scene.control.TableColumn._
import scalafx.scene.layout.HBox
import scalafx.stage.Stage


case class Co[T](f: T => Unit) extends Consumer[T]
{
  override  def accept(t: T) { f(t)}
}

object Exchanges
{
  val sysInfo = Map("bitfinex" -> classOf[BitfinexStreamingExchange])
  val stopList = Nil
  var loaded = Map[String, Exchange]()

  def load =
  {
    sysInfo.foreach(es => if (!stopList.contains(es._1))
      loaded += es._1 -> Exchange(es._1, sysInfo(es._1).getName))

    loaded.values
  }
  load
}

case class Exchange(name: String, className: String)
{
  //all money in usd
  def amount = 0d
  def status = false
  val exchange = StreamingExchangeFactory.INSTANCE.createExchange(className)
  // Connect to the Exchange WebSocket API. Blocking wait for the connection.
  exchange.connect().blockingAwait()

  val streamService = exchange.getStreamingMarketDataService

  exchange.connectionSuccess().subscribe(Co(x => subscribeCurrencies()), Co(throwable => {}))

  val pair2bookInfo = mutable.Map[CurrencyPair, BookInfo]()
  val specs = exchange.getExchangeSymbols
  val hasBots = Config.whereBots.find(_ == name).isDefined
  //fill pair2bookInfo
  specs.foreach(p => if (Config.currencies.contains(p.base.toString) &&
    p.counter.toString == "USD") pair2bookInfo += p -> BookInfo(this, p))

  val currency2Info = pair2bookInfo.map(pi => (pi._1.base.toString, pi._2))

  def pairSubscribe(pair: CurrencyPair)
  {
    val bookInfo = pair2bookInfo(pair)

    bookInfo.subscription = streamService.getOrderBook(pair)
      .subscribe(Co(orderBook => pair2bookInfo(pair).update(orderBook)),
        Co(throwable => {
          Main.exception(s"Subscribing orderbook for $pair: $throwable")
        }))
    bookInfo.tradeSubscription = streamService.getTrades(pair)
      .subscribe(Co(trade => {
        pair2bookInfo(pair).inTrade(trade)
      }), Co(throwable => {
        Main.exception(s"Error in subscribing trades for $pair: $throwable")
      }))
  }
  def subscribeCurrencies()
  {
    specs.foreach(p => if (Config.currencies.contains(p.base.toString) &&
      p.counter.toString == "USD") pairSubscribe(p))
  }
  subscribeCurrencies()

  def unsubscribeCurrencies()
  {
    pair2bookInfo.foreach(pi => if (pi._2.subscription != null)
      {
        pi._2.subscription.dispose()
        pi._2.subscription = null
        pi._2.tradeSubscription.dispose()
        pi._2.tradeSubscription = null
      })
  }
  def ratesUpdate(timeRates: Seq[TimeRate]): Unit =
    timeRates.foreach(timeRate => currency2Info(timeRate.param).update(timeRate))

  def play(si: SeriesInfo): Unit =
  {
    pair2bookInfo.foreach(pi => pi._2.clear())
    Db.readSeriesInfo(si, ratesUpdate)
  }
  def dispose()
  {
    unsubscribeCurrencies()
    exchange.disconnect().subscribe()
  }
}

object ExchangesTable
{
  val buffer = new ObservableBuffer[ExchangeView]()

  def update(exchange: Exchange) = buffer.find(_.name == exchange.name).map(_.update(exchange))

  val viewPoint = new TableView[ExchangeView](buffer)
  {
    columns ++= List(
      new TableColumn[ExchangeView, String] {
        text = "Name"
        cellValueFactory = _.value.name
        prefWidth = 90
      },
      new TableColumn[ExchangeView, Boolean] {
        text = "Status"
        cellValueFactory = _.value.status
        prefWidth = 90
      },
      new TableColumn[ExchangeView, Double] {
        text = "amount"
        cellValueFactory = _.value.amount
        prefWidth = 90
      },
      new TableColumn[ExchangeView, Button] {
        text = "Board"
        cellValueFactory = _.value.board
        cellFactory = {
          _: TableColumn[ExchangeView, Button] => new TableCell[ExchangeView, Button] {
            item.onChange { (_, _, newButton) =>
              graphic = newButton
            }
          }
        }
      },
      new TableColumn[ExchangeView, Button] {
        text = "Record"
        cellValueFactory = _.value.start
        cellFactory = {
          _: TableColumn[ExchangeView, Button] => new TableCell[ExchangeView, Button] {
            item.onChange { (_, _, newButton) =>
              graphic = newButton
            }
          }
        }
      },
      new TableColumn[ExchangeView, Button] {
        text = "Series"
        cellValueFactory = _.value.series
        cellFactory = {
          _: TableColumn[ExchangeView, Button] => new TableCell[ExchangeView, Button] {
            item.onChange { (_, _, newButton) =>
              graphic = newButton
            }
          }
        }
      }

    )
    editable = false
  }
  viewPoint.setPrefHeight(400)
  viewPoint.setPrefWidth(682)

  //!!!Exchanges.load.foreach(e => buffer.append(ExchangeView(e)))

  val boxBooks = new HBox()

  val scrollPane = new ScrollPane{
    hbarPolicy = ScrollBarPolicy.AsNeeded
    vbarPolicy = ScrollBarPolicy.AsNeeded
    content = boxBooks
  }

  var pairViews : Seq[PairView] = Nil

  var shownExchange: Exchange = null

  def addPairInfo(bookInfo: BookInfo) =
  {
    val pv = PairView(bookInfo)
    boxBooks.children.append(pv.viewPoint)
    pv
  }

  def updateOrderBook(bookInfo: BookInfo)
  {
    if (shownExchange == bookInfo.exchange)
      pairViews.find(_.bookInfo == bookInfo).foreach(_.update())
  }

  def clearBoxBooks()
  {
    boxBooks.children.clear()
    pairViews = Nil
    shownExchange = null
  }

  def showChart(bookInfo: BookInfo)
  {
    val list = List("small", "middle", "big") ++ Config.baseTrendSumString
    val chart = MiddleChart(bookInfo.currency.toString, list).window(bookInfo.exchange.name)
    bookInfo.subscribe(chart)
  }


  def showOrderBooks(exchange: Exchange)
  {
    clearBoxBooks()
    pairViews = exchange.pair2bookInfo.map(pi => addPairInfo(pi._2)).toSeq
    shownExchange = exchange

    val stageOrderBook = new Stage{
      title.value = exchange.name + " OrderBook info"
      width = 1800
      height = 1000
      scene = new Scene {
        content = scrollPane
      }
    }
    stageOrderBook.setOnCloseRequest(_ =>  clearBoxBooks())
    stageOrderBook.show
    scrollPane.setMaxWidth(stageOrderBook.getWidth)
  }
}

