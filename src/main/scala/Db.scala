/**
  * Created by Георгий on 16.11.2018.
  */
package agent

import java.time.Instant

import com.paulgoldbaum.influxdbclient.Parameter.{Consistency, Precision}
import com.paulgoldbaum.influxdbclient._

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Await
import scala.concurrent.duration.Duration


object Db extends Notify[BookInfo] {
  implicit class PointExt(val point: Point) extends AnyVal
  {
    def add(trate: TimeRate, nameRates: Seq[String]): Point = add(trate.rates, nameRates)

    def add(rates: Array[Double], nameRates: Seq[String]): Point =
      nameRates.zip(rates).foldLeft(point)((p,nr) => p.addField(nr._1, nr._2))

    def add(sp: StrategyParams): Point = 
      point.addField("low",sp.low.value.get).addField("middle",sp.middle.value.get).addField("high",sp.high.value.get).
        addField("intense",sp.intense.value.get).addField("threshold",sp.threshold.value.get).addField("delay",
        sp.delay.value.get)
    
  }

  val historyFields = List("rate","volume")
  val namesMiddles = (0 until 400).map(_.toString).toArray

  def writeHistory(trates: Seq[TimeRate], exchange: String, middles: ArrayBuffer[Array[Double]])
  {
    val points = trates.map(tr => Point("history", tr.time.toEpochMilli).add(tr, historyFields).addTag("exchange", exchange))
    points.zip(middles).map(pm => pm._1.add(pm._2, namesMiddles)).foreach(writePoint(_)) //writePoints doesn't work here!
  }

  def writeStrategyParams(end: Instant, tag: String, strategyParams: StrategyParams, percents: Double) =
    writePoint(Point("strategy", end.toEpochMilli).add(strategyParams).addField("percents", percents).addTag("type",tag))


  def writeEffects(start: Instant, effect: Double, maxEffect: Double) =
    writePoint(Point("strategy", start.toEpochMilli).addField("effect", effect).addField("maxEffect", maxEffect))

  def readStrategyParams(start: Instant, end: Instant, tag: String = null): mutable.Map[Instant, (StrategyParams, Double)] =
  {
    var qstr = s"""SELECT * FROM "strategy" WHERE time >= ${start.toEpochMilli}$nulls AND
         time <= ${end.toEpochMilli}$nulls"""
    if (tag != null)
      qstr += s" AND tag = '$tag'"

    Await.result(rates.query(qstr, precMilli).map(
      result =>
        {
          var map = mutable.Map.empty[Instant, (StrategyParams, Double)]
          result.series.head.records.foreach(r => map += timeRec(r) -> rec2StrategyPercent(r))
          map
        }
      ), Duration.Inf)
  }

  def rec2double(rec: Record, key: String): Double = rec(key).asInstanceOf[BigDecimal].toDouble

  def rec2rates(rec: Record, keys: Array[String]): Array[Double] = keys.map(rec2double(rec,_))

  def historyQuery(exchange: String, start: Instant, end: Instant, currency: String = null) = {
      var str = s"""SELECT * FROM "history" WHERE time >= ${start.toEpochMilli}$nulls AND
         time < ${end.toEpochMilli}$nulls AND exchange = '${exchange}'"""
      if (currency != null)
        str += s" AND currency = '$currency'"
      rates.query(str, precMilli).map(result => {
        val arr = mutable.ArrayBuffer[TimeRate]()
        if (!result.series.isEmpty)
          result.series.head.records.foreach(r => arr += TimeRate(timeRec(r),
            rec2rates(r, namesMiddles), rec2double(r,"volume")))
        arr
      })
  }

  def historyCallback(exchange: String, start: Instant, end: Instant, callback: Seq[TimeRate] => Unit, currency: String = null) =
  {
    historyQuery(exchange, start, end, currency).onComplete({
      case Success(result) => callback(result)
      case Failure(exception) =>
        Main.exception(exception)
    })
  }

  def timeRec(rec: Record, fieldName: String = "time"): Instant =
    Instant.ofEpochMilli(rec(fieldName).asInstanceOf[BigDecimal].toLong)

  def rec2StrategyPercent(rec: Record) = {
    import genotype.Genetic._
    def intParam(name: String) = intGene(rec(name).asInstanceOf[BigDecimal].toInt)
    def doubleParam(name: String) = doubleGene(rec(name).asInstanceOf[BigDecimal].toDouble)
    (StrategyParams(intParam("low"), intParam("middle"), intParam("high"),
      doubleParam("intense"), doubleParam("threshold"), intParam("delay")),
      rec("percents").asInstanceOf[BigDecimal].toDouble)
  }

  val db = InfluxDB.connect("localhost", 8086)

  def close = db.close()

  val pointsBuffer = mutable.ArrayBuffer[Point]()
  val precMilli = Precision.MILLISECONDS
  val nulls = "000000" //for adding in query and make nanosec from millisec
  val rates = db.selectDatabase("rates")

  val activeSeries = mutable.Map[String, Point]()

  def getTimeRangeHistory =
  {
    def getF(side: String) = rates.query(s"""SELECT $side("0") FROM "history"""", precMilli).
      map(f => timeRec(f.series.head.records(0)))
    Await.result(getF("First").zip(getF("Last")), Duration.Inf)
  }

  def series(exchange: String, callback: Seq[SeriesInfo] => Unit): Unit =
  {
    rates.query(s"""SELECT * FROM "series" WHERE exchange = '$exchange'""", precMilli).onComplete({
      case Success(result) =>
        callback(if (result.series.isEmpty) Nil else
          result.series.head.records.map(r => {
            SeriesInfo(exchange, timeRec(r),
              timeRec(r,"end"), r("currencies").toString.split(','))
          }))

      case Failure(exception) =>
        Main.exception(exception)
    })
  }

  def ratePoint(exchange: String, currency: String, trate: TimeRate, nameRates: Seq[String]) =
    Point("sets", trate.time.toEpochMilli).addTag("exchange",exchange).addTag("currency", currency).add(trate, nameRates)

  def writePoints(points: Point*): Unit ={
    rates.bulkWrite(points, precision = precMilli, consistency = Consistency.ALL)
  }
  def writePoint(point: Point): Unit ={
    rates.write(point, precision = precMilli, consistency = Consistency.ALL)
  }

  def stopWriteSeries(exchange: Exchange): Unit ={
    exchange.pair2bookInfo.values.foreach(bi => bi.unsubscribe(this))
  }

  def readSeriesInfo(seriesInfo: SeriesInfo, callback: Seq[TimeRate] => Unit, currency: String = null) =
  {
    var qstr = s"""SELECT * FROM "sets" WHERE time >= ${seriesInfo.start.toEpochMilli}$nulls AND
         time <= ${seriesInfo.end.toEpochMilli}$nulls AND exchange = '${seriesInfo.exchange}'"""
    if (currency != null)
      qstr += s" AND currency = '$currency'"
    rates.query(qstr, precMilli).onComplete({
      case Success(result) =>
        if (result.series.isEmpty)
          Main.warning(s"Empty series for $seriesInfo")
        else
        {
          val arr = mutable.ArrayBuffer[TimeRate]()
          result.series.head.records.foreach(r => arr += TimeRate(timeRec(r), rec2rates(r,Config.baseTrendSumString),0d,
              r("currency").toString))
          callback(arr)
        }
      case Failure(exception) =>
        Main.exception(exception)
    })
  }
  def writeSeriesInfo(seriesInfo: SeriesInfo): Unit ={
    val point = Point("series", seriesInfo.start.toEpochMilli).addField("end", seriesInfo.end.toEpochMilli)
    writePoint(point.addField("exchange",seriesInfo.exchange).addField("currencies", seriesInfo.currencies.mkString(",")))
    addCheckWriteCashPoints(null,true)
  }

  def startWriteSeries(exchange: Exchange)
  {
    exchange.pair2bookInfo.values.foreach(bi => bi.subscribe(this))
  }
  def addCheckWriteCashPoints(point: Point, force: Boolean = false)
  {
    if (point != null)
      pointsBuffer.append(point)
    if (pointsBuffer.size > Config.cashPointsSize || force)
    {
      writePoints(pointsBuffer:_*)
      pointsBuffer.clear()
    }
  }
  def updated(bookInfo: BookInfo): Unit ={
    val p = ratePoint(bookInfo.exchange.name,bookInfo.currency.toString,
      TimeRate(bookInfo.lastTr.time, bookInfo.middles.last_rates.toArray), Config.baseTrendSumString)
    addCheckWriteCashPoints(p)
  }
}
