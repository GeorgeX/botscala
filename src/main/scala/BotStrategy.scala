package agent
import java.time.temporal.ChronoUnit

import agent.BotsStrategiesCalculator.dateRange
import genotype.Configuration.{EvolutionConfig, PopulationConfig}
import genotype.Genes.{DoubleGene, IntGene}
import genotype.Genetic.defaultBotStrategyParams
import genotype.{EvolutionFactory, Genetic}

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

object BotStrategy {
  import java.io._

  def serialize(arr: Array[StrategyParams], name: String = "stparams.oos"): Unit = {
    val oos = new ObjectOutputStream(new FileOutputStream(name))
    oos.writeObject(arr)
    oos.close
  }

  def deserialize(name: String = "stparams.oos"): Array[StrategyParams] = {
    val ois = new ObjectInputStream(new FileInputStream(name))
    val arr = ois.readObject.asInstanceOf[Array[StrategyParams]]
    ois.close
    arr
  }
  def calcWriteBestStrategies(lenMinutes: Int) =
    BotStrategy.serialize(calcBestStrategies(lenMinutes * 60).toArray, s"stp$lenMinutes")

  def test() {

    //calcWriteBestStrategies(1000)
    //calcWriteBestStrategies(2000)
    //calcWriteBestStrategies(4000)
    calcBestsOfAll()
  }

  def calcBestMetaStrategy(): Unit ={
    val (_, end) = dateRange
    val begin = end.minusSeconds(3600 * 24 * 2)
    val trs = Await.result(Db.historyQuery("bitfinex", begin, end), Duration.Inf)
    MetaBot.getBestMetaBot4(trs)
  }

  lazy val bestStrategies = deserialize("bestOrder").take(100) // deserialize("stp1000") ++ deserialize("stp2000") ++ deserialize("stp4000")

  def calcBestsOfAll(stepTimeSec: Int = 2 * 24 * 3600) = {

    val bots = bestStrategies.map(params => new BotStrategyMix(params)).toList.toParArray
    val (_, end) = dateRange
    val begin = end.minusSeconds(40 * 24 * 3600)
    val steps = (ChronoUnit.SECONDS.between(begin, end) / stepTimeSec).toInt
    val accounts = Array.fill[Accounts](bots.size)(new Accounts())
    var last: TimeRate = null
    var first: TimeRate = null

    for (i <- 1 until steps)
    {
      val from = begin.plusSeconds(stepTimeSec * i)
      val to = from.plusSeconds(stepTimeSec)
      val trs = Await.result(Db.historyQuery("bitfinex", from, to), Duration.Inf)

      if (first == null)
        first = trs(0)

      bots.zipWithIndex.map(bi => bi._1.totalSum4(trs, accounts(bi._2)))
      last = trs.last
    }
    val result = accounts.map(_.currentSum(last.rate)).zip((0 until bots.size).map(i => bots(i).params)).sortBy(-_._1)
    val firstSum = new Accounts().currentSum(first.rate)

    result.map(r => println(s"${r._1/firstSum} ${r._2}"))
    serialize(result.map(_._2).take(200), "bestOrder")
  }

  def evalBots(trs: Seq[TimeRate]) = EvolutionFactory(
    candidate = defaultBotStrategyParams,
    populationConfig = PopulationConfig(500),
    evolutionConfig = EvolutionConfig(100),
    errorRate = (c: StrategyParams) => {errorBot(c, trs)}
  ).run()


  def optimalStrategy4(trs: Seq[TimeRate]) =
  {
    val strategy = evalBots(trs)(0)
    strategy
  }

  def errorBot(s: StrategyParams, trs: Seq[TimeRate]) = {
    val sum = new BotStrategyMix(s).totalSum4(trs,Accounts())
    - sum
  }
  def timeSeq(stepTimeSec: Int, duration: Int = 0) =
  {
    val (begin, end) = dateRange
    val steps = (ChronoUnit.SECONDS.between(begin, end) / stepTimeSec).toInt

    for (i <- 1 until steps) yield {
      val from = begin.plusSeconds(stepTimeSec * i)
      val to = from.plusSeconds(if (duration != 0) duration else stepTimeSec)
      (from, to)
    }
  }
  def calcBestStrategies(stepTimeSec: Int, duration: Int = 0) =
  {
    val strategies = ArrayBuffer[StrategyParams]()

    for (fromTo <- timeSeq(stepTimeSec, duration))
    {
      val trs = Await.result(Db.historyQuery("bitfinex", fromTo._1, fromTo._2), Duration.Inf)
      strategies += BotStrategy.optimalStrategy4(trs)
    }
    strategies
  }
}

@SerialVersionUID(130L)
case class StrategyParams(low: IntGene, middle: IntGene, high: IntGene, intense: DoubleGene,
                          threshold: DoubleGene, delay: IntGene)

class BotStrategyMix(val params : StrategyParams = Genetic.defaultBotStrategyParams,
    operations: OperationsLog = new OperationsLog ) extends Bot(null) with Strategy
{
  minDelayBetweenOperationsSec = params.delay.value.get

  strategy = this
  val intense = params.intense.value.get
  val middlesIndexes = List(params.low,params.middle,params.high).map(ip=> ip.value.get)
  var minMax: Double = 0d
  var lastTrendAction: Double = 0d
  var lastTrend: Double = 0d

  override def clear(){
    super.clear()
    minMax = 0
    lastTrendAction = 0
    lastTrend = 0
  }
  override def operationCompete: Unit =
  {
    super.operationCompete
    minMax = 0
    lastTrendAction = lastTrend
  }
  val indexes = Array(params.low.value.get,params.middle.value.get,params.high.value.get)

  override def middleValues() : Seq[Double] = indexes.map(lastTr.rates(_))

  def retValue(value: Double) = if (value > 0.5) 0.5 else if (value < -0.5) -0.5 else value

  def trendValue(trMiddles: TimeRate) : Double =
  {
    val diffs = middlesIndexes.map(i => trMiddles.rate / trMiddles.rates(i))
    var prev = 1d

    for(e <- diffs)
      if (e <= prev)
        prev = e
      else
        prev = 0

    if (prev != 0)
      (1 - diffs.fold(1d)((a,b) => a * b)) * intense
    else
    {
      prev = 1d
      for(e <- diffs)
        if (e >= prev && prev != 0)
          prev = e
        else
          prev = 0
      if (prev == 0)
        0
      else
        (1 - diffs.fold(1d)((a,b) => a * b) ) * intense
    }
  }
  val threshold = params.threshold.value.get

  def localMinMax(d: Double) =
  {
    var ret = false
    if (minMax == 0 && (d > threshold || d < -threshold))
      minMax = d
    else
      if (minMax > 0)
        if (d > minMax)
          minMax = d
        else
          ret = true
      else
        if (minMax < 0)
          if (d < minMax)
            minMax = d
          else
            ret = true
    if ((minMax >= 0) != (d >= 0))
      minMax = 0
    ret
  }
  //return 0 or positive [0, 1] for buying or negative [0,-1] for selling part of crypto active
  override def action(rateVolume: TimeRate): Double =
  {
    super.action(rateVolume)
    if (possibleOperation)
    {
      lastTrend = trendValue(rateVolume)
      if (localMinMax(lastTrend))
        retValue(lastTrend)
      else
        0
    }
    else
      0
  }
}
