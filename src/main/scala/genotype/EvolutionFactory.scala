package genotype

import agent.MetaStrategy
import genotype.Configuration.{EvolutionConfig, PopulationConfig}
import genotype.evolution.{Breeder, Generator, Mutator}

import scala.annotation.tailrec
import scala.util.Random

object EvolutionFactory{
  var logging = false
}

case class EvolutionFactory[A](candidate: A,
  populationConfig: PopulationConfig,
  evolutionConfig: EvolutionConfig,
  errorRate: A => Double)
  (implicit gen: Generator[A], mut: Mutator[A], breeder: Breeder[A]) {

  def run(population: Seq[A] = Nil) = {
    @tailrec
    def evolutionCycle(pop: List[A], generation: Int): List[A] = {
      if (generation > evolutionConfig.generations) return pop

      val tops = pop.toParArray.map(c => (c, errorRate(c))).toList.sortBy(_._2).take(populationConfig.size/2).map(_._1)

      if (EvolutionFactory.logging || candidate.isInstanceOf[MetaStrategy])
        println(s"Gen $generation, top ${tops.head}")

      val nextGen = tops.head :: mutate(tops) ::: evolve(tops)

      evolutionCycle(nextGen, generation+1)
    }

    def mutate(list: List[A]): List[A] = list.map(mut.mutate)

    def evolve(seq: List[A]): List[A] =
    {
      def randomElem = seq(Random.nextInt(seq.size))

      (0 to (populationConfig.size / 2)).map (_ => breeder.breed(randomElem, randomElem)).toList
    }
    val initialPop = if (population.isEmpty) generateCandidates() else population

    evolutionCycle(initialPop.toList, 0)
  }

  private def generateCandidates(popSize: Int = populationConfig.size) = {
    (0 to popSize).map(i => gen.generate(candidate))
  }
}
