package genotype

object Genes {

  sealed trait Gene[A] { val value: Option[A] }
  @SerialVersionUID(125L)
  case class IntGene(min: Int = Int.MinValue/2, max: Int = Int.MaxValue/2, value: Option[Int] = None) extends Gene[Int]
  @SerialVersionUID(126L)
  case class DoubleGene(min: Double = Double.MinValue/2, max: Double = Double.MaxValue/2, value: Option[Double] = None) extends Gene[Double]
  @SerialVersionUID(127L)
  case class BooleanGene(value: Option[Boolean] = None) extends Gene[Boolean]

}
