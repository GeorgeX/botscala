package agent


import java.time.{Instant, LocalDateTime, ZoneId}
import java.time.temporal.ChronoUnit

import ml.dmlc.xgboost4j.scala.{Booster, DMatrix, XGBoost}
import org.knowm.xchange.dto.marketdata.OrderBook

import scala.collection.mutable.ArrayBuffer


object Utils {

  def array(initVal: Double = 0) = Array.fill[Double](Config.trendCount)(initVal)

  def slice[T](seqSeq: Seq[Seq[T]])= {
    for(i <- seqSeq(0).indices)
      yield seqSeq.map(_(i))
  }

  def timeFormat(time: Instant, mode: String = "minute"): String = {
    val localDateTime = LocalDateTime.ofInstant(time, ZoneId.systemDefault())

    mode match {
      case "day" => s"${localDateTime.getDayOfMonth}/${localDateTime.getMonthValue}/${localDateTime.getYear-2000}"
      case "hour" => s"${localDateTime.getHour}:${localDateTime.getMinute} ${localDateTime.getDayOfMonth}/${localDateTime.getMonthValue}"
      case "minute" => s"${localDateTime.getHour}:${localDateTime.getMinute}"
    }
  }

  def extremums(seq: Seq[TimeRate]) =
  {
    val result = ArrayBuffer[TimeRate]()
    var min = seq(0)
    var max = seq(0)
    val rmin = seq.map(_.rate).min
    val rmax = seq.map(_.rate).max
    var lookForMax = false
    var lastEx = seq(0).rate

    def minDiffExremumsSuits = (max.rate / min.rate - 1) >= Config.minDiffExremums

    for(tr <- seq)
    {
      if (tr.rate > max.rate)
        max = tr
      else
        if (tr.rate < min.rate)
          min = tr

      //if (rate(min) == rmin && rate(max) == rmax)
      //  min = min

      if (min.rate < lastEx && min.rate < max.rate && min.time.isAfter(max.time) && minDiffExremumsSuits) {
        if (lookForMax || result.isEmpty) {
          result += max
          lastEx = max.rate
          max = min
          lookForMax = false
        }
      }
      else
        if (max.rate > lastEx && lastEx > min.rate && max.time.isAfter(min.time) && minDiffExremumsSuits)
          if (!lookForMax || result.isEmpty) {
            result += min
            lastEx = min.rate
            min = max
            lookForMax = true
          }
    }
    //add last max and min points if diff suits
    if (minDiffExremumsSuits)
      result.append(if (lookForMax) max else min, if (lookForMax) min else max)
    result
  }

  def chartData(seq: Seq[TimeRate],from: Instant, to: Instant): Array[Array[Double]] = {
    var arr = Array[Array[Double]]()
    val iter = seq.iterator
    var prevTr = iter.next
    var currTr = prevTr

    val ratesDimension  = seq(0).rates.size

    val maxVals, minVals = Array.ofDim[Double](ratesDimension)

    var time = from

    def setTrs() {
      maxVals.indices.foreach(i => maxVals(i) = 0)
      minVals.indices.foreach(i => minVals(i) = Double.MaxValue)

      while (currTr.time.isBefore(time) && iter.hasNext) {
        //calc max
        maxVals.indices.foreach(i => if (maxVals(i) < currTr.rates(i)) maxVals(i) = currTr.rates(i))
        //calc min
        minVals.indices.foreach(i => if (minVals(i) > currTr.rates(i)) minVals(i) = currTr.rates(i))

        prevTr = currTr
        currTr = iter.next
      }
      if (currTr.time.equals(time)) //no range prev, curr if time == time in rate
        prevTr = currTr

      if (maxVals(0) == 0)
        maxVals.indices.foreach(i => if (maxVals(i) < currTr.rates(i)) maxVals(i) = currTr.rates(i))
      if (minVals(0) == Double.MaxValue)
        minVals.indices.foreach(i => if (minVals(i) > currTr.rates(i)) minVals(i) = currTr.rates(i))
    }

    setTrs()

    if (iter.hasNext)
    {
      val diffFromTo = ChronoUnit.MILLIS.between(from, to)
      var range = if (to.isAfter(seq.last.time))
        (Config.chartRange * ChronoUnit.MILLIS.between(from, seq.last.time) / diffFromTo).toInt
      else Config.chartRange

      val stepMs = diffFromTo / Config.chartRange

      arr = Array.ofDim[Double](ratesDimension,range)

      var index = 0

      range -= 1

      while(currTr != null && index < range)

        if (!currTr.time.isBefore(time)) {
          for(i <- arr.indices) {
            arr(i)(index) = maxVals(i)
            arr(i)(index + 1) = minVals(i)
          }
          time = time.plusMillis(stepMs * 2)
          index += 2
        }
        else
        if (iter.hasNext)
          setTrs
        else
          currTr = null
    }
    arr
  }

  def timeRate4OrderBook(orderBook: OrderBook): TimeRate = {
    val iterAsk = orderBook.getAsks.iterator()

    val ask = iterAsk.next()

    val iterBids = orderBook.getBids.iterator()

    val bid = iterBids.next()

    TimeRate(orderBook.getTimeStamp.toInstant, Array(ask.getLimitPrice.doubleValue() + bid.getLimitPrice.doubleValue() / 2))
  }

  def orderBook2Trades(orderBook: OrderBook): TimeRate =
  {
    val iterAsk =  orderBook.getAsks.iterator()

    var amount = 0d

    var money4amount = 0d

    val rates = array()

    var save_curr_rate_amount: (Double,Double) = null

    for(i <- Config.baseTrendSum.indices)
    {
      val baseSum = Config.baseTrendSum(i)

      while(rates(i) == 0 && (save_curr_rate_amount != null || iterAsk.hasNext)) {

        val (curr_rate, curr_amount) = if (save_curr_rate_amount == null)
        {
          val ask = iterAsk.next()
          (ask.getLimitPrice.doubleValue(), ask.getRemainingAmount.doubleValue())
        }
        else {
          val curr_rate_amount = save_curr_rate_amount //restore saved rest from prev consuming

          save_curr_rate_amount = null

          curr_rate_amount
        }

        val amountCurrencyInDollar =  curr_rate * curr_amount

        if (money4amount + amountCurrencyInDollar >= baseSum) {
          val addingAmount = (baseSum - money4amount) / curr_rate

          assert(addingAmount > 0)

          rates(i) = baseSum / (amount + addingAmount)

          if (i > 0 && rates(i) < rates(i -1) && rates(i) - rates(i - 1) < 0.000000000001)
            rates(i) = rates(i - 1)

          amount += addingAmount

          money4amount += curr_rate * addingAmount

          save_curr_rate_amount = (curr_rate, curr_amount - addingAmount)
        }
        else {
          amount += curr_amount

          money4amount += amountCurrencyInDollar
        }
      }
    }
    TimeRate(Instant.now, rates)
  }
}

object XBoost {
  val paramMap = Map("eta" -> 0.1, "max_depth" -> 2, "objective" -> "binary:logistic")

  def trainData(dataFile: String) = new DMatrix(dataFile)

  def trainModel(dataFile: String = "/path/to/agaricus.txt.train", iterations: Int = 2) =
    XGBoost.train(trainData(dataFile), paramMap, iterations)

  def saveModel(model: Booster, filename: String) = model.saveModel(filename)

  def predict(model: Booster, data: DMatrix) = model.predict(data)

  def makeTrainData(arr: Array[Array[Double]]) =
    arr.map{subarr => {
      val first = subarr(0) match {
        case 0.0 => "0"
        case 1.0 => "1"
        case other  => s"1:$other"
      }
      first + subarr.zipWithIndex.drop(1).map{case (e,j) => s" $j:$e"}.mkString
    }}.mkString("\n")

}
