package agent

import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.layout._


object Config
{
  import scala.io.Source

  val json = Source.fromFile("config.json").mkString

  val root = ujson.transform(json, ujson.Value)

  val currencies = root("currencies").arr.map(_.str)

  def exchangeParam(nameExchange: String, key: String) = root("exchanges")(nameExchange).obj.get(key)

  def dbmsParam(key: String) = root("dbms").obj.get(key)

  val bunchWriteSize = dbmsParam("bunchWriteSize").map(_.num).getOrElse(10d)

  val cashPointsSize = dbmsParam("cashPointsSize").map(_.num.toInt).getOrElse(20)

  val transactionInterval = root("bot")("transactionInterval").num

  val whereBots = root("bot")("where").arr.map(_.str)

  val baseTrendSum = root("trend")("baseSums").arr.map(_.num)

  val tickPeriod = root("trend")("tickPeriod").num.toInt

  val baseTrendSumString = baseTrendSum.map(_.toInt.toString).toArray

  val trendPeriods = root("trend")("periods").arr.map(_.num.toInt)

  val trendCount = trendPeriods.size

  val chartRange = root("chart")("range").num.toInt

  val keepCashMs = root("parameters")("keepCashMs").num.toLong

  val range2Probe = root("trend")("range2Probe").arr.map(v => (v.arr(0).num.toInt , v.arr(1).num)).toMap

  val minDiffExremums = root("parameters")("minDiffExremums").num
}



object Main  extends JFXApp{

  val guiActive = true

  def callback(seqRates: Seq[TimeRate]) = {
    println("ok")
  }

  //BotStrategy.calcBestMetaStrategy()
  //BotStrategy.calcBestsOfAll()
  //BotsStrategiesCalculator.calcMapBestStrategies
  //BotsStrategiesCalculator.calcWriteEffectParameters(80000)

  val boxMain = new HBox()

  boxMain.children.add(ExchangesTable.viewPoint)

  stage = new JFXApp.PrimaryStage {
    title.value = "Exchange info"
    width = 680
    height = 600
    scene = new Scene {
      content = boxMain
    }
  }

  stage.setOnCloseRequest(_ =>  System.exit(0))

  def exception(e: Object): Unit ={
    println("Exception: " + e)
  }

  def warning(e: Object): Unit ={
    println("Warning: " + e)
  }

  HistoryChart("Currency rate","hist").window("History rates")
}