/**
  * Created by Георгий on 05.11.2018.
  */
package agent

import java.time.Instant
import java.time.temporal.ChronoUnit

import io.reactivex.disposables.Disposable
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.marketdata.{OrderBook, Trade}

import scala.collection.mutable
import scala.collection.JavaConversions._

case class RateMs(value: Double, var duration: Long)

case class Middle(durationMs: Long)
{
  val buf = mutable.Queue[RateMs]()
  var bufDuration = 0L
  var bufSum = 0d
  var lastTr: TimeRate = _

  def current = bufSum / bufDuration

  def clear(): Unit =
  {
    buf.clear()
    bufDuration = 0
    bufSum = 0
    lastTr = null
  }

  def update(tr: TimeRate) =
  {
    if (lastTr == null) {
      lastTr = tr
      tr.rate
    }
    else {
      val passedTime = ChronoUnit.MILLIS.between(lastTr.time, tr.time)
      lastTr = tr
      if (passedTime + bufDuration > durationMs)
      {
        var removeMs = passedTime + bufDuration - durationMs

        while(!buf.isEmpty && buf(0).duration <= removeMs)
        {
          val rateMs = buf.dequeue()
          bufSum -= rateMs.value * rateMs.duration
          removeMs -= rateMs.duration
        }
        if (!buf.isEmpty && removeMs > 0)
        {
          bufSum -= buf(0).value * removeMs
          buf(0).duration -= removeMs
        }
        bufDuration = durationMs
      }
      else
        bufDuration = passedTime + bufDuration

      val duration4save = if (passedTime > durationMs) durationMs else passedTime
      bufSum += tr.rate * duration4save
      buf += RateMs(tr.rate, duration4save)
      current
    }
  }
}

case class Middles(periods: Seq[Int] = Config.trendPeriods.map(_ * Config.tickPeriod))
{
  val middles = periods.map(Middle(_))//Config.trendPeriods.map(t => Middle(t * Config.tickPeriod))
  var last_rates: Seq[Double] = _

  def clear() = middles.foreach(_.clear())

  def update(tr: TimeRate) = {
    last_rates = middles.map(_.update(tr))
    last_rates
  }
  def updateRangeMinutes(tr: TimeRate): Boolean = {
    val middle1Min = middles(0)
    val lastTr1min =   middle1Min.lastTr
    if (lastTr1min == null || ChronoUnit.SECONDS.between(lastTr1min.time, tr.time) >= 1) {
      update(tr)
      true
    }
    else
      false
  }
}

case class Trends(keepCashMs: Long = Config.keepCashMs)
{
  var buf = mutable.Queue[TimeRate]()
  def update(middleTr: TimeRate) : TimeRate =
  {
    if (buf.isEmpty)
      buf += TimeRate(middleTr.time, Utils.array(1))
    else
      if (!middleTr.time.equals(buf.last.time)) { //nanos ignored by hardware
        assert(middleTr.time.isAfter(buf.last.time))

        val middles = middleTr.rates.map(identity)
        for (i <- Config.trendPeriods.indices)
          middles(i) /= buf.last.rates(i)
        val tm = TimeRate(middleTr.time, middles)
        buf += tm
        while (ChronoUnit.MILLIS.between(buf.front.time, buf.last.time) > keepCashMs)
          buf.dequeue()
      }
    buf.last
  }
}

case class BookInfo(exchange: Exchange, pair: CurrencyPair, keepCashMs: Long = Config.keepCashMs)
  extends Dispatcher[BookInfo]
{
  var orderBook : OrderBook = _
  var subscription: Disposable = _
  var tradeSubscription: Disposable = _
  var lastTr: TimeRate = _
  val middles = Middles((1 to 400).map(_ * 1000 * 60))

  //load middles
  val history4middles = History.get(exchange.name, Instant.now.getEpochSecond, pair.base.toString).map(_.arr).getOrElse(null)
  assert(history4middles != null)
  middles.clear() //update could be called

  history4middles.foreach(middles.updateRangeMinutes(_))

  lazy val bot = if (exchange.hasBots) {
      val metaBot = new MetaBot(MetaBot.bestMetaStrategy)//
      val iterAsk = orderBook.getAsks.iterator()
      assert(iterAsk.hasNext)
      metaBot.init(orderBook.getTimeStamp.toInstant, iterAsk.next().getLimitPrice.doubleValue())
      metaBot
    }
  else null

  val trends = Trends(keepCashMs)
  val currency = pair.base
  def balance = 0d

  def clear(): Unit =
  {
    orderBook = null
    if (subscription != null)
    {
      subscription.dispose()
      subscription = null
    }
    lastTr = null
    middles.clear()
    trends.buf.clear()
  }
  def inTrade(trade: Trade): Unit =
  {
    //println(trade)
  }
  def update(orderBook_ : OrderBook)
  {
    if (subscription != null)
      update(Utils.timeRate4OrderBook(orderBook_), orderBook_)
  }
  def update(tr: TimeRate, orderBook_ : OrderBook = null)
  {
    lastTr = tr

    orderBook = orderBook_

    try {
      val updated = middles.updateRangeMinutes(lastTr)
      //if (bot != null && (updated || bot)
      //trends.update(middleTr)
    }
    catch
    {
      case e : Throwable => e.printStackTrace()
    }
    dispatch
    //ExchangesTable.updateOrderBook(this)
  }
}

