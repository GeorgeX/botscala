package agent


import java.time.format.DateTimeFormatter

import scala.collection.mutable.ArrayBuffer
import java.time.{Instant, LocalDateTime, ZoneId}

import genotype.EvolutionFactory
import scalafx.collections.ObservableBuffer
import scalafx.Includes._
import scalafx.scene.control._
import scalafx.scene.layout.{HBox, VBox}

abstract class RatesHistory
{
  def arr: ArrayBuffer[TimeRate]
  def maxVolume: Double
  def seqPartVolumes: ArrayBuffer[Double]
}

case class RatesHistoryBitfinex(arrJs: ArrayBuffer[ujson.Value], mode: String) extends RatesHistory
{
  lazy val size = arrJs.value.size
  var maxVolume = 0d

  val arr = arrJs.map(e =>
  {
    val a = e.arr
    val rate = a(2).num
    val volumeTo = a(5).num
    if (volumeTo > maxVolume)
      maxVolume = volumeTo
    val time = Instant.ofEpochMilli(a(0).num.toLong)
    TimeRate(time, Array(rate, volumeTo), volumeTo)
  })

  lazy val seqPartVolumes = arr.map(_.rates(1) /maxVolume)
  override def toString (): String = arr.map(e => s"${Utils.timeFormat(e.time, mode)} rate: ${e.rate}, volume:${e.volume}").mkString("\n")
}

case class RatesDbHistory(trs: Seq[TimeRate]) extends RatesHistory
{
  val arr = ArrayBuffer(trs:_*)
  val volumes = arr.map(_.volume)
  val maxVolume = volumes.max
  lazy val seqPartVolumes = volumes.map(_ / maxVolume)
}

object History{
  val modes = List("minute","hour","day")
  var mode = modes(2)
  var limit: Int = 2000

  //bitfinex only
  def get(exchange: String = "bitfinex", lastStamp: Long = 1452680400, currency: String = "BTC") :Option[RatesHistory] =
  {
    assert(exchange == "bitfinex")
    val start = Instant.ofEpochSecond(lastStamp).minusSeconds(limit * 60).getEpochSecond * 1000 //ms
    val query = s"https://api-pub.bitfinex.com/v2/candles/trade:1m:t${currency}USD/hist?sort=1&" +
      s"limit=$limit&end=${lastStamp*1000}&start=$start"
    val result = requests.get(query)
    if (result.statusCode != 200)
      None
    else
      Some(new RatesHistoryBitfinex(ujson.read(result.text).arr, mode))
  }
}

case class HistoryChart(info: String, header: String ) extends LChart(info,
  Config.chartRange, List("rate", "accum actions", "extremums") ++ Config.trendPeriods.map(_.toString): _*)
{
  var seq: ArrayBuffer[TimeRate] = _
  val volumeChart = new LChart("volume", Config.chartRange, "volume $", "metaB","extremeB")

  def convertVolumesAndUpdate(rh: RatesHistory, mainStrategy: Strategy)
  {
    val rates = rh.arr.map(_.rates(0))
    val minRate = rates.reduce((a,b) => if (a < b) a else b)
    val maxRate = rates.reduce((a,b) => if (a > b) a else b)
    val diffRate = (maxRate - minRate) / 2
    val middleRate = diffRate + minRate
    val extremums = Utils.extremums(rh.arr)
    val ratesMarks = for(i <- rh.arr.indices) yield{
      val tr = rh.arr(i)
      val extremumLine = extremums.find(_.time.equals(tr.time)).map(_.rates(0)).getOrElse(middleRate)
      val middlesRate = mainStrategy.action(tr)
      TimeRate(tr.time, Array(tr.rate, middleRate + diffRate * middlesRate, extremumLine) ++ mainStrategy.middleValues())
    }

    update(ratesMarks)
    mainStrategy.clear()

    //bot process volumes
    val mainBot = new Bot(mainStrategy)
    val mainBotAccounts = Accounts()
    //metabot
    EvolutionFactory.logging = true
    val metaBot = new MetaBot(MetaBot.bestMetaStrategy)
    val fElem = rh.arr(0)
    metaBot.init(fElem.time.minusMillis(1000), fElem.rate)
    EvolutionFactory.logging = false
    ////end metabot
    val seqMirrorTotal = rh.arr.map(metaBot.process(_))
    mainStrategy.clear()

    val extremeBot = new BotStrategyMix(BotStrategy.optimalStrategy4(rh.arr)) //new Bot(new ExtremumStrategy(Utils.extremums(rh.arr)))
    val extremeBotAccounts = Accounts()

    val seqExtremeTotal = rh.arr.map(extremeBot.process(_, extremeBotAccounts)).map(ac => ac._1 + ac._2)

    val min = Seq(seqMirrorTotal.min, seqExtremeTotal.min).min
    val max = Seq(seqMirrorTotal.max, seqExtremeTotal.max).max

    val diffMinMax = max-min
    val middle = diffMinMax / 2 + min

    val botRates = (0 until ratesMarks.size).map(i => TimeRate(ratesMarks(i).time,
      Array(min +  rh.seqPartVolumes(i) * diffMinMax, seqMirrorTotal(i), + seqExtremeTotal(i))))

    volumeChart.update(botRates)
  }
  override def scale(up: Boolean, ratio: Double, e: Any): Unit ={
    super.scale(up, ratio, e)
    volumeChart.scale(up, ratio, e)
  }
  def toggleSelectedText(tg: ToggleGroup) =
    tg.selectedToggle.value.asInstanceOf[javafx.scene.control.RadioButton].getText

  def window(info: String) =
  {
    val exchanges = Exchanges.sysInfo.keys
    val bufExchanges = new ObservableBuffer[String]()
    exchanges.foreach(bufExchanges.append(_))

    val comboExchange = new ComboBox[String] {
      maxWidth = 200
      promptText = "Choose exchange..."
      items = bufExchanges
      value = "bitfinex"
    }

    val bufCurrencies = new ObservableBuffer[String]()
    Config.currencies.foreach(bufCurrencies.append(_))

    val comboCurrency = new ComboBox[String] {
      maxWidth = 200
      promptText = "Choose currency..."
      items = bufCurrencies
      value = "BTC"
    }
    val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
    val date = new TextField {
      text = LocalDateTime.now().format(dateFormatter)
      promptText = "Date time"
      maxWidth = 200
      text = "2019-02-15 13:39"
    }
    val limit = new TextField {
      promptText = "Limit"
      maxWidth = 100
      text = History.limit.toString
    }
    val tog = new ToggleGroup()
    val togDbApi = new ToggleGroup()
    val DbApiButtons = List(
      new RadioButton {
        maxWidth = 200
        maxHeight = 50
        text = "api"
        toggleGroup = togDbApi
      },
      new RadioButton {
        maxWidth = 200
        maxHeight = 50
        text = "influx"
        selected = true
        toggleGroup = togDbApi
      })
    val button = new Button("Chart")
    button.onAction = handle
    {
      val time = LocalDateTime.parse(date.getText, dateFormatter)
      val end = time.toInstant(ZoneId.systemDefault().getRules().getOffset(time))
      val timeStamp = end.getEpochSecond
      History.limit = limit.getText.toInt
      History.mode = toggleSelectedText(tog)

      if (toggleSelectedText(togDbApi) == "api")
        History.get(comboExchange.getValue, timeStamp, comboCurrency.getValue).foreach(h =>
          convertVolumesAndUpdate(h, new MiddleStrategy()))
      else
        Db.historyCallback("bitfinex", end.minusSeconds(History.limit * 60), end,
          {trs => convertVolumesAndUpdate(RatesDbHistory(trs), new BotStrategyMix())})
    }
    val radioButtons = List(
      new RadioButton {
        maxWidth = 200
        maxHeight = 50
        text = "minute"
        selected = true
        toggleGroup = tog
      },
      new RadioButton {
        maxWidth = 200
        maxHeight = 50
        text = "hour"
        toggleGroup = tog
      },
      new RadioButton {
        maxWidth = 200
        maxHeight = 50
        text = "day"
        toggleGroup = tog
      })

    val boxMain = new VBox()
    val toolBox = new HBox()
    toolBox.children = radioButtons ++ DbApiButtons
    toolBox.children.addAll(limit, comboExchange, comboCurrency, date,  button)
    boxMain.children.addAll(toolBox, lineChart, volumeChart.lineChart)
    Window(info, boxMain)
    this
  }
}
