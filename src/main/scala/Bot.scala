package agent

import java.time.Instant
import java.time.temporal.ChronoUnit

import scala.collection.mutable

case class Account(currency: String, var sum: Double, minSumOperation: Double = 0.0000000000001d)
{
}

case class AccountOperation(time: Instant, sumMain: Double, sumUsd: Double, rate: Double)
{
}

trait Strategy
{
  var minDelayBetweenOperationsSec: Long = 5
  var lastOperationTr: TimeRate = _
  var lastTr: TimeRate = _

  //call if bot did the operation
  def operationCompete: Unit ={
    lastOperationTr = lastTr
  }

  def possibleOperation: Boolean =
    lastOperationTr == null || lastTr == null || ChronoUnit.SECONDS.between(lastOperationTr.time, lastTr.time) >=
      minDelayBetweenOperationsSec

  //return 0 or positive for buying or negative for selling crypto
  def action(rateVolume: TimeRate): Double =
  {
    lastTr = rateVolume
    0
  }
  def clear(){
    lastOperationTr = null
    lastTr = null
  }
  def middleValues() : Seq[Double] = Nil
}

class MiddleStrategy(minDelayBetweenOpsSec_ : Long = 5) extends Strategy
{
  minDelayBetweenOperationsSec = minDelayBetweenOpsSec_
  val middles = Middles()
  val mult = 50

  override def clear(){
    super.clear()
    middles.clear()
    minMax = 0
  }

  override def middleValues() : Seq[Double] = middles.last_rates

  def retValue(value: Double) = if (value > 0.5) 0.5 else if (value < -0.5) -0.5 else value

  def trendValue(rateVolume: TimeRate) : Double = {
    val diffs = middles.last_rates.map(rateVolume.rate / _)
    var prev = 1d

    for(e <- diffs)
      if (e <= prev)
        prev = e
      else
        prev = 0

    if (prev != 0)
      - (diffs.fold(1d)((a,b) => a * b) - 1) * mult
    else
    {
      prev = 1d
      for(e <- diffs)
        if (e >= prev && prev != 0)
          prev = e
        else
          prev = 0
      if (prev == 0)
        0
      else
        (1 - diffs.fold(1d)((a,b) => a * b) ) * mult
    }
  }

  var minMax: Double = 0d
  val threshold = 0.2d

  def localMinMax(d: Double) =
  {
    var ret = false
    if (minMax == 0 && (d > threshold || d < -threshold))
      minMax = d
    else
      if (minMax > 0)
        if (d > minMax)
          minMax = d
        else
          ret = true
      else
        if (minMax < 0)
          if (d < minMax)
            minMax = d
          else
            ret = true
      if ((minMax >= 0) != (d >= 0))
        minMax = 0

      ret
  }

  //return 0 or positive [0, 1] for buying or negative [0,-1] for selling part of crypto active
  override def action(rateVolume: TimeRate): Double =
  {
    middles.update(rateVolume)

    if (possibleOperation)
    {
      val tv = trendValue(rateVolume)
      if (localMinMax(tv))
        retValue(tv)
      else
        0
    }
    else
      0
  }
}

class ExtremumStrategy(extremums: Seq[TimeRate]) extends Strategy
{
  minDelayBetweenOperationsSec = 1
  //return 0 or positive for buying or negative for selling crypto
  override def action(rateVolume: TimeRate): Double =
  {
    val rate = extremums.zipWithIndex.find(_._1.time == rateVolume.time).getOrElse((0d,0))
    val firstIsMaximum = extremums(0).rate > extremums(1).rate
    if (rate._1 != 0)
      if (firstIsMaximum ^ rate._2 % 2 == 0) 1 else -1
    else
      0
  }
}

class OperationsLog {
  def += (op: AccountOperation): Unit = {}
}

class LogBuffer extends OperationsLog{
  val operations = mutable.ArrayBuffer[AccountOperation]()
  override def += (op: AccountOperation): Unit ={
    operations += op
  }
}

case class Accounts(var mainSum: Double = 1, var usdSum: Double = 3400)
{
  def currentSum(rate: Double) = mainSum * rate + usdSum
  def copy: Accounts = Accounts(mainSum, usdSum)

  def checkAndWithdrawMain(sum: Double, lessPossible: Double = 0.0000001d) =
  {
    if (mainSum >= sum) {
      mainSum -= sum
      sum
    }
    else
      if (lessPossible > 0 && mainSum > lessPossible)
      {
        val takenSum = mainSum
        mainSum = 0
        takenSum
      }
      else
        0d
  }
  def checkAndWithdrawUsd(sum: Double, lessPossible: Double = 0.0000001d) =
  {
    if (usdSum >= sum) {
      usdSum -= sum
      sum
    }
    else
    if (lessPossible > 0 && usdSum > lessPossible)
    {
      val takenSum = usdSum
      usdSum = 0
      takenSum
    }
    else
      0d
  }
}

class Bot(var strategy: Strategy,  operations: OperationsLog = new OperationsLog ) {

  val restFine = 0.999d

  def totalSum4(trs: Seq[TimeRate], accounts: Accounts) = {

    var lastTotal = (0d, 0d)
    trs.map(tr => lastTotal = process(tr, accounts))
    lastTotal._1 + lastTotal._2
  }

  def process(rateVolume: TimeRate, accounts: Accounts) =
  {
    val part = strategy.action(rateVolume)

    if (part > 0) {
      val sum = accounts.usdSum * part
      val opSum = accounts.checkAndWithdrawUsd(sum)
      if (opSum > 0)
      {
        val sum2Main = opSum * restFine / rateVolume.rate
        accounts.mainSum += sum2Main
        operations += AccountOperation(rateVolume.time, sum2Main, -opSum, rateVolume.rate)
        strategy.clear()
        strategy.operationCompete
      }
    }
    else if (part < 0) {
      val sum = -accounts.mainSum * part
      val opSum = accounts.checkAndWithdrawMain(sum)
      if (opSum > 0)
      {
        val sum2Usd = opSum * restFine * rateVolume.rate
        accounts.usdSum += sum2Usd
        operations += AccountOperation(rateVolume.time, -opSum, sum2Usd, rateVolume.rate)
        strategy.clear()
        strategy.operationCompete
      }
    }
    (accounts.mainSum * rateVolume.rate, accounts.usdSum)
  }
}

