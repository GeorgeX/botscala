/**
  * Created by Георгий on 20.10.2018.
  */

package genotype

import Genes._
import agent._
import genotype.evolution.Generator

import scala.util.Random

object Genetic{
  def createIntGene(min: Int, max: Int, value: Option[Int] = None) = IntGene(min, max,
    if (value.isDefined) value else Some(Random.nextInt(max - min) + min))

  def intGene(value: Int) = createIntGene(0, value, Some(value))

  def createDoubleGene(min: Double, max: Double, value: Option[Double] = None) =
    DoubleGene(min, max, if (value.isDefined) value else Some((Random.nextDouble * (max - min)) + min))

  def doubleGene(value: Double) = createDoubleGene(0, value, Some(value))

  implicit def StrategyParamsGenerator = new Generator[StrategyParams] {

    override def generate(a: StrategyParams) = {
      val low = createIntGene(2, 40)
      val middle  = createIntGene(low.value.get + 10, 100)
      val high = createIntGene(middle.value.get + 10, 399)
      StrategyParams(low, middle, high, createDoubleGene(0,500),
        createDoubleGene(0,1), createIntGene(0,50000))
    }
  }
  lazy val defaultBotStrategyParams =
    StrategyParams(IntGene(2,40,Some(34)),IntGene(21,100,Some(87)),IntGene(61,399,Some(137)),
      DoubleGene(0.0,500.0,Some(187.49774022286482)),DoubleGene(0.0,1.0,Some(0.6801899144338827)),
      IntGene(0,50000,Some(17797)))

  implicit def MetaStrategyParamsGenerator = new Generator[MetaStrategy]
  {
    override def generate(a: MetaStrategy) = MetaStrategy(createDoubleGene(0,10),
      createDoubleGene(0,10), createDoubleGene(0,10), createDoubleGene(0,1000),
      createIntGene(2,2000), createIntGene(30,400))

    def generateDefault() = {
      generate(null)
    }
  }
  lazy val defaultMetaBotStrategyParams = MetaStrategyParamsGenerator.generateDefault
}
