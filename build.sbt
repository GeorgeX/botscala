name := "scalabot"

version := "1.0"

scalaVersion := "2.12.7"

val xchangeV = "4.3.11"

resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"

libraryDependencies ++= Seq(
  "javax.ws.rs" % "javax.ws.rs-api" % "2.1" artifacts Artifact("javax.ws.rs-api", "jar", "jar"),
  "org.knowm.xchange" % "xchange-core" % xchangeV,
  "org.knowm.xchange" % "xchange-livecoin" % xchangeV,
  "org.knowm.xchange" % "xchange-bitfinex" % xchangeV,
  "org.knowm.xchange" % "xchange-poloniex" % xchangeV,
  "info.bitrich.xchange-stream" % "xchange-stream-core" % xchangeV,
  "info.bitrich.xchange-stream" % "xchange-bitfinex" % xchangeV,
  "info.bitrich.xchange-stream" % "xchange-poloniex" % xchangeV,
  "com.lihaoyi" %% "ujson" % "0.7.1",
  "com.lihaoyi" %% "requests" % "0.1.6",
  "com.paulgoldbaum" %% "scala-influxdb-client" % "0.6.1",
  "org.slf4j" % "slf4j-api" % "1.7.25",
  "org.slf4j" % "slf4j-simple" % "1.7.25",
  "ml.dmlc" % "xgboost4j" % "0.81",
  "com.chuusai" %% "shapeless" % "2.3.3"
)

libraryDependencies += "org.scalafx" %% "scalafx" % "11-R16"

// Determine OS version of JavaFX binaries
lazy val osName = System.getProperty("os.name") match {
  case n if n.startsWith("Linux")   => "linux"
  case n if n.startsWith("Mac")     => "mac"
  case n if n.startsWith("Windows") => "win"
  case _ => throw new Exception("Unknown platform!")
}

lazy val javaFXModules = Seq("base", "controls", "fxml", "graphics", "media", "swing", "web")
libraryDependencies ++= javaFXModules.map( m =>
  "org.openjfx" % s"javafx-$m" % "11" classifier osName
)
    